<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_names', function (Blueprint $table) {
            $table->id();
            $table->string('list_name')->nullable();
            $table->string('code_country')->nullable();
        });
        
        DB::table('country_names')->insert(
        [
            ['list_name'=>'Cambodia','code_country'=>'855'],
            ['list_name'=>'Thailand','code_country'=>'66'],
            ['list_name'=>'Vietnam','code_country'=>'84'],
            ['list_name'=>'Singapore','code_country'=>'65'],
            ['list_name'=>'Myanmar','code_country'=>'95'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_names');
    }
}
