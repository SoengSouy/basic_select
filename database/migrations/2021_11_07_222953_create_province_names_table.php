<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvinceNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province_names', function (Blueprint $table) {
            $table->id();
            $table->string('list_name_pro')->nullable();
            $table->string('code_country')->nullable();
        });
        DB::table('province_names')->insert(
        [
            ['list_name_pro' => 'Banteay Meanchey','code_country'=>'855'],
            ['list_name_pro' => 'Battambang','code_country'=>'855'],
            ['list_name_pro' => 'Kampong Cham','code_country'=>'855'],
            ['list_name_pro' => 'Kampong Chhnang','code_country'=>'855'],
            ['list_name_pro' => 'Kampong Speu','code_country'=>'855'],
            ['list_name_pro' => 'Kampong Thom','code_country'=>'855'],
            ['list_name_pro' => 'Kampot','code_country'=>'855'],
            ['list_name_pro' => 'Kandal','code_country'=>'855'],
            ['list_name_pro' => 'Koh Kong','code_country'=>'855'],
            ['list_name_pro' => 'Kratié','code_country'=>'855'],
            ['list_name_pro' => 'Mondulkiri','code_country'=>'855'],
            ['list_name_pro' => 'Phnom Penh','code_country'=>'855'],
            ['list_name_pro' => 'Preah Vihear','code_country'=>'855'],
            ['list_name_pro' => 'Prey Veng','code_country'=>'855'],
            ['list_name_pro' => 'Pursat','code_country'=>'855'],
            ['list_name_pro' => 'Ratanak Kiri','code_country'=>'855'],
            ['list_name_pro' => 'Siem Reap','code_country'=>'855'],
            ['list_name_pro' => 'Preah Sihanouk','code_country'=>'855'],
            ['list_name_pro' => 'Stung Treng','code_country'=>'855'],
            ['list_name_pro' => 'Svay Rieng','code_country'=>'855'],
            ['list_name_pro' => 'Takéo','code_country'=>'855'],
            ['list_name_pro' => 'Oddar Meanchey','code_country'=>'855'],
            ['list_name_pro' => 'Kep','code_country'=>'855'],
            ['list_name_pro' => 'Pailin','code_country'=>'855'],
            ['list_name_pro' => 'Tboung Khmum','code_country'=>'855'],
            ['list_name_pro' => 'Th_Province 1','code_country'=>'66'],
            ['list_name_pro' => 'Th_Province 2','code_country'=>'66'],
            ['list_name_pro' => 'Th_Province 3','code_country'=>'66'],
            ['list_name_pro' => 'Th_Province 4','code_country'=>'66'],
            ['list_name_pro' => 'Th_Province 5','code_country'=>'66'],
            ['list_name_pro' => 'Vi_Province 1','code_country'=>'84'],
            ['list_name_pro' => 'Vi_Province 2','code_country'=>'84'],
            ['list_name_pro' => 'Vi_Province 3','code_country'=>'84'],
            ['list_name_pro' => 'Vi_Province 4','code_country'=>'84'],
            ['list_name_pro' => 'Vi_Province 5','code_country'=>'84'],
            ['list_name_pro' => 'Si_Province 1','code_country'=>'65'],
            ['list_name_pro' => 'Si_Province 2','code_country'=>'65'],
            ['list_name_pro' => 'Si_Province 3','code_country'=>'65'],
            ['list_name_pro' => 'Si_Province 4','code_country'=>'65'],
            ['list_name_pro' => 'Si_Province 5','code_country'=>'65'],
            ['list_name_pro' => 'My_Province 1','code_country'=>'95'],
            ['list_name_pro' => 'My_Province 2','code_country'=>'95'],
            ['list_name_pro' => 'My_Province 3','code_country'=>'95'],
            ['list_name_pro' => 'My_Province 4','code_country'=>'95'],
            ['list_name_pro' => 'My_Province 5','code_country'=>'95'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('province_names');
    }
}
