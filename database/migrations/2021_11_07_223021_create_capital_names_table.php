<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapitalNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capital_names', function (Blueprint $table) {
            $table->id();
            $table->string('list_name_cap')->nullable();
            $table->string('code_country')->nullable();
        });
        DB::table('capital_names')->insert(
        [
            ['list_name_cap' => 'Chamkar Mon','code_country'=>'855'],
            ['list_name_cap' => 'Daun Penh','code_country'=>'855'],
            ['list_name_cap' => 'Prampir Makara','code_country'=>'855'],
            ['list_name_cap' => 'Tuol Kork','code_country'=>'855'],
            ['list_name_cap' => 'Dangkao','code_country'=>'855'],
            ['list_name_cap' => 'Mean Chey','code_country'=>'855'],
            ['list_name_cap' => 'Russey Keo','code_country'=>'855'],
            ['list_name_cap' => 'Sen Sok','code_country'=>'855'],
            ['list_name_cap' => 'Th_capital 1','code_country'=>'66'],
            ['list_name_cap' => 'Th_capital 2','code_country'=>'66'],
            ['list_name_cap' => 'Th_capital 3','code_country'=>'66'],
            ['list_name_cap' => 'Th_capital 4','code_country'=>'66'],
            ['list_name_cap' => 'Th_capital 5','code_country'=>'66'],
            ['list_name_cap' => 'Vi_capital 1','code_country'=>'84'],
            ['list_name_cap' => 'Vi_capital 2','code_country'=>'84'],
            ['list_name_cap' => 'Vi_capital 3','code_country'=>'84'],
            ['list_name_cap' => 'Vi_capital 4','code_country'=>'84'],
            ['list_name_cap' => 'Vi_capital 5','code_country'=>'84'],
            ['list_name_cap' => 'Si_capital 1','code_country'=>'65'],
            ['list_name_cap' => 'Si_capital 2','code_country'=>'65'],
            ['list_name_cap' => 'Si_capital 3','code_country'=>'65'],
            ['list_name_cap' => 'Si_capital 4','code_country'=>'65'],
            ['list_name_cap' => 'Si_capital 5','code_country'=>'65'],
            ['list_name_cap' => 'My_capital 1','code_country'=>'95'],
            ['list_name_cap' => 'My_capital 2','code_country'=>'95'],
            ['list_name_cap' => 'My_capital 3','code_country'=>'95'],
            ['list_name_cap' => 'My_capital 4','code_country'=>'95'],
            ['list_name_cap' => 'My_capital 5','code_country'=>'95'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capital_names');
    }
}
