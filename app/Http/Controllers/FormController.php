<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class FormController extends Controller
{
    // index page
    public function index()
    {
        $country = DB::table('country_names')->get();
        $province = DB::table('province_names')->get();
        $capital = DB::table('capital_names')->get();
        return view('formselect',compact('country','province','capital'));
    }
}
